## [0.0.3] - 2021-03-26

### Added

- :construction_worker: Adding gitlab-CI for deploying in Heroku

### Changed

- :wrench: Updating CHANGELOG.md

- :wrench: Updating README.md

## [0.0.2] - 2021-03-26

### Changed

- :wrench: Updating README.md

## [0.0.1] - 2021-03-26

### Changed

- :wrench: Updating API key

### Added 

- :memo: Adding the CHANGELOG.md
