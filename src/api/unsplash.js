import axios from 'axios';

export default axios.create({
  baseURL: 'https://api.unsplash.com',
  headers: {
    Authorization:
      'Client-ID vhRtzFFIIv1h8TGEp-iV0Wa304NXCLmbIF9dDbg2xTY'
  }
});
